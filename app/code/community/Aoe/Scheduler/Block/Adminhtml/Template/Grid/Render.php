<?php
class Aoe_Scheduler_Block_Adminhtml_Template_Grid_Render extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $codes = $row->getId();
        $src = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN);
        $skinSrc = $this->getSkinUrl();
        $hrefRunNow = $this->getUrl('*/*/runNow');
        $hrefDelete = $this->getUrl('*/*/Disable');
        $out = "<a class='Aoe-disable' href=".$hrefDelete."?codes=$codes"."><img src=".$skinSrc."aoe_scheduler/Images/disabled.png" ." width='20px'/></a>";
        $out .= "<a class='Aoe-runNow' style='margin-left: 10px;' href=".$hrefRunNow."?codes=$codes"."><img src=".$skinSrc."aoe_scheduler/Images/play.png" ." width='20px'/</a>";
        return  $out;

    }
}

